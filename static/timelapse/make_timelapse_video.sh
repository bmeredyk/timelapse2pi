#avconv -r 10 -i pics/image_%04d.jpg -r 10 -vcodec libx264 -crf 20 -g 15 timelapse.mp4
#-r is framerate in frames/second
# -i input filename
# -vcodec set video coded (alias for "-codec:v")
# -g set group of picture (GOP) size
# -crf Select the quality for constant quality mode

avconv -r 10 -i image_%04d.jpg -r 10 -vcodec libx264 -crf 20 -g 15 timelapse.mp4

