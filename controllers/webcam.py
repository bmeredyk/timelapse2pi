# -*- coding: utf-8 -*-
# try something like
import time
import picamera
import os
from datetime import datetime


    
def index(): 
    start_time = datetime.now()
    with picamera.PiCamera() as camera:
        #camera.start_preview()
        #time.sleep(1)
        camera.capture(os.path.join(request.folder, 'static', 'webcam', 'latest.jpg'))
        #camera.stop_preview()
        camera.close()
    end_time = datetime.now()
    return dict(message="hello from webcam.py", run_time = end_time - start_time, start_time = start_time, end_time = end_time)
