# -*- coding: utf-8 -*-
# try something like
import time
#import picamera
import os
from datetime import datetime
import time

try:
    import json
except ImportError:
    from gluon.contrib import simplejson as json

def index(): 
    timelapses = db((db.timelapse.id > 0) & (db.timelapse.scheduler_task == db.scheduler_task.id)).select()
    return dict(timelapses = timelapses, message="hello from timelapse.py")

def create():
    interval = int(request.vars.interval) or 15
    number_of_frames = request.vars.frames
    duration = request.vars.duration
    try:
        hres = int(request.vars.hres)
    except:
        hres = 720
    try:
        vres = int(request.vars.vres)
    except:
        vres = 480
        
    
    if duration != None:
        number_of_frames = int(int(duration)/interval)
    elif duration == None and number_of_frames == None:
        #default to 10 frames
        number_of_frames = 10
    
    #first delete existing images
    images = os.listdir(os.path.join(request.folder, 'static', 'timelapse'))
    for image in images:
        os.remove(os.path.join(request.folder, 'static', 'timelapse', image))
    
    frames_taken = 0
    with picamera.PiCamera() as camera:
        camera.resolution = (hres,vres)
        
        #camera.start_preview()
        #time.sleep(1)
        while frames_taken < number_of_frames:
            frames_taken +=1
            camera.capture(os.path.join(request.folder, 'static', 'timelapse', 'snapshot_%04d.jpg' % frames_taken))
            time.sleep(interval)
        #camera.stop_preview()
        camera.close()
    #get contents of directory
    image_files = sorted(os.listdir(os.path.join(request.folder, 'static', 'timelapse')))
    
    images = []
    for image in image_files:
        stats = os.stat(os.path.join(request.folder, 'static', 'timelapse', image))
        images.append(dict(
                        filename = image, 
                        stats = stats, 
                        created_time = datetime.fromtimestamp(stats.st_ctime)
                        )
                      )
        
    
    return dict(images = images, interval = interval, number_of_frames = number_of_frames)

def pipic():
    interval = int(request.vars.interval) or 10
    number_of_frames = request.vars.frames
    duration = int(request.vars.duration)
    try:
        hres = int(request.vars.hres)
    except:
        hres = 720
    try:
        vres = int(request.vars.vres)
    except:
        vres = 480
        
    
    if duration != None:
        number_of_frames = int(int(duration)/interval)
    elif duration == None and number_of_frames == None:
        #default to 10 frames
        number_of_frames = 10

    
    #first delete existing images
    images = os.listdir(os.path.join(request.folder, 'static', 'timelapse'))
    for image in images:
        os.remove(os.path.join(request.folder, 'static', 'timelapse', image))
    

    #run the timelapse
    run_timelapse(interval = interval, interval_uom = 'seconds', 
                    maxtime = duration, maxtime_uom = 'seconds',
                    include_exif = True,
                    brightness = 90,
                    width = hres, height = vres
                    )  
    #get list of images taken
    #get contents of directory
    image_files = sorted(os.listdir(os.path.join(request.folder, 'static', 'timelapse')))
    images = []
    for image in image_files:
        stats = os.stat(os.path.join(request.folder, 'static', 'timelapse', image))
        images.append(dict(
                        filename = image, 
                        stats = stats, 
                        created_time = datetime.fromtimestamp(stats.st_ctime)
                        )
                      )
        
    
    return dict(images = images, interval = interval, number_of_frames = number_of_frames)
    
def queue_lapse():
    try:
        interval = int(request.vars.interval)
    except:
        interval = 10

    try:
        duration = int(request.vars.duration)
    except:
        duration = 5

    try:
        if request.vars.interval_uom in ['second','seconds','minute','minutes','hour','hours']:
            interval_uom = request.vars.interval_uom
        else:
            interval_uom = 'seconds'
    except:
        interval_uom = 'seconds'

    try:
        if request.vars.duration_uom in ['second','seconds','minute','minutes','hour','hours']:
            duration_uom = request.vars.duration_uom
        else:
            duration_uom = 'minutes'
    except:
        duration_uom = 'minutes'
    
    try:
        height = int(request.vars.height)
    except:
        height = 480
    try:
        width = int(request.vars.width)
    except:
        width = 720

    try:
        output_dir = request.vars.output_dir
    except:
        output_dir = datetime.now().strftime('%Y-%m-%d_%H.%M')
    if output_dir == None:
        output_dir = datetime.now().strftime('%Y-%m-%d_%H.%M')
    maxtime = interval_to_seconds(duration, duration_uom)
    
    try:
        name = request.vars.name
    except:
        name="My Timelapse"

    task = scheduler.queue_task(
        'raspistill_lapse',
        pargs=[],
        pvars=dict(width=width, height=height, duration=duration, duration_uom=duration_uom,
                    interval=interval, interval_uom=interval_uom, output_directory = output_dir,
                    name=name, base_dir = request.folder, brightness = 90),
        start_time=datetime.now(), 		#datetime
        stop_time = None,		#datetime
        timeout = maxtime+60,               #seconds
        prevent_drift=False,
        period=60,                  #seconds
        immediate=True,
        repeats = 1
        )
    
    #save timelapse to database so can monitor/view images later
    timelapse_id = db.timelapse.insert(name = name, scheduler_task = task, image_folder = output_dir)

    redirect(URL(r=request, f='view', args=[timelapse_id]))
    return dict(task = task, maxtime = maxtime)
    
def schedule():
    """Schedule a timelapse using the standard raspistill timelapse feature"""
    form = SQLFORM(db.timelapse, fields = ['name', 'image_folder', 
                    'shot_interval', 'shot_uom', 'duration', 'duration_uom',
                    'width', 'height', 'start_time', 'custom_raspistill_args'])
    if form.process().accepted:
        response.flash = 'form accepted'
        new_id = form.vars.id
        timelapse = db.timelapse[new_id]
        #queue up task for scheduler to run

        maxtime = interval_to_seconds(timelapse.duration, timelapse.duration_uom)

        task = scheduler.queue_task(
            'raspistill_lapse',
            pargs=[],
            pvars=dict(width=timelapse.width, height=timelapse.height, duration=timelapse.duration, 
                        duration_uom=timelapse.duration_uom,
                        interval=timelapse.shot_interval, interval_uom=timelapse.shot_uom, 
                        output_directory = timelapse.image_folder,
                        name=timelapse.name, base_dir = request.folder,
                        timelapse_id = timelapse.id, custom_raspistill_args = timelapse.custom_raspistill_args),
            start_time=timelapse.start_time, 		#datetime
            stop_time = None,		#datetime
            timeout = maxtime+60,               #seconds
            prevent_drift=False,
            period=60,                  #seconds
            #immediate=True,
            repeats = 1
            )
        
        #save timelapse to database so can monitor/view images later
        timelapse.update_record(scheduler_task = task)
        redirect(URL(r=request, f='view', args=[timelapse.id]))


    elif form.errors:
        response.flash = 'form has errors'
    else:
        response.flash = 'please fill out the form'
    

    return dict(form = form)

def view():
    #print('viewing timelapse %s' % request.args[0])
    timelapse = db.timelapse[request.args[0]]
    
    try:
        preview_images = int(request.vars.preview_images)
    except:
        preview_images = 10#how many recent images to show in addition to latest
    
    task = db.scheduler_task[timelapse.scheduler_task]
    timelapse_settings = json.loads(task.vars)
    
    #get list of images taken
    #get contents of directory
    image_directory = os.path.join(request.folder,'static', 'timelapse', timelapse.image_folder)

    if task.status in ['RUNNING', 'COMPLETED', 'FAILED']:
        #if just queued task may need to wait for it to be created
        
        if not os.path.exists(image_directory):
            os.makedirs(image_directory)#create the directory now (actual timelapse hasn't started yet)
        
        image_files = sorted(os.listdir(image_directory))
        images = []
        for image in image_files:
            if image != 'latest.jpg':
                stats = os.stat(os.path.join(image_directory, image))
                images.append(dict(
                                filename = image, 
                                stats = stats, 
                                created_time = datetime.fromtimestamp(stats.st_ctime)
                                )
                              )
    else:
        images = []

    #response.files.append('http://cdn.ractivejs.org/latest/ractive.js')
    #response.files.append('//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.3/moment-with-locales.min.js')
    #use our local version since sometimes operate without actual internet connection
    response.files.append(URL('static', 'js/ractive.js'))
    response.files.append(URL('static', 'js/moment-with-locales.min.js'))

    return dict(timelapse = timelapse, timelapse_settings = timelapse_settings,
            images = images, task = task, preview_images = preview_images, image_directory = image_directory)

def download():
    timelapse = db.timelapse[request.args[0]]

    task = db.scheduler_task[timelapse.scheduler_task]
    timelapse_settings = json.loads(task.vars)
    image_directory = os.path.join(request.folder,'static', 'timelapse', timelapse.image_folder)
    zip_filename = timelapse.image_folder+"_timelapse_images.zip"
    zip_file_path = os.path.join(request.folder,'static', 'timelapse', zip_filename)

    #only create if doesn't already exist (or not forced to)
    if not os.path.exists(zip_file_path) or request.vars.rebuild:
        #we're dealing with jpeg images so don't bother trying to compress,
        #pretty much a waste of time & CPU on our poor rpi
        compression = zipfile.ZIP_STORED#if want compression try zipfile.ZIP_DEFLATED
        zipf = zipfile.ZipFile(zip_file_path,
                                "w", compression=compression, allowZip64 = True)
        recursive_zip(zipf, image_directory) 
        #also add shell script to convert into a video via avconvert
        #(Just not practical to try to do it on the rpi with its whimpy CPU)
        zipf.write(os.path.join(request.folder,'static', 'timelapse', 'make_timelapse_video.sh'),'make_timelapse_video.sh')
        zipf.close()
    redirect(URL(r=request,c='static', f='timelapse/%s' % zip_filename))

def download_tar():
    #creates a tar file with all of the images from a timelapse
    timelapse = db.timelapse[request.args[0]]

    task = db.scheduler_task[timelapse.scheduler_task]
    timelapse_settings = json.loads(task.vars)
    image_directory = os.path.abspath(os.path.join(request.folder,'static', 'timelapse', timelapse.image_folder))
    tar_filename = timelapse.image_folder+"_timelapse_images.tar"
    tar_file_path = os.path.abspath(os.path.join(request.folder,'static', 'timelapse', tar_filename))
    timelapse_script = os.path.abspath(os.path.join(request.folder,'static', 'timelapse', 'make_timelapse_video.sh'))

    #only create if doesn't already exist (or not forced to)
    if not os.path.exists(tar_file_path) or request.vars.rebuild:
        #we're dealing with jpeg images so don't bother trying to compress,
        #pretty much a waste of time & CPU on our poor rpi
        
        #import tarfile
        #tar_file = tarfile.open(tar_file_path, "w")
        #tar_file.add(image_directory)
        #tar_file.add(os.path.join(request.folder,'static', 'timelapse', 'make_timelapse_video.sh'))
        
        #use good old tar command line since using tarfile module seems to often create corrupt archives when have lots of images
        subprocess.call('tar -cf '+tar_file_path+' -C '+image_directory+' . -C '+image_directory+'/../../helper_scripts/ .', shell=True)
        #subprocess.call('tar -rf '+tar_file_path+' '+timelapse_script, shell=True)

    redirect(URL(r=request,c='static', f='timelapse/%s' % tar_filename))

def get_latest_image():
    """gets the most recent image taken by a timelapse job"""
    timelapse = db.timelapse[request.args[0]]

    task = db.scheduler_task[timelapse.scheduler_task]
    timelapse_settings = json.loads(task.vars)
    image_file = os.path.join(request.folder,'static', 'timelapse/'+ timelapse.image_folder+ '/latest_image.jpg')

    if task.status == 'RUNNING':
        #going to stream it ourselves rather than just redirecting so can avoid caching.
        redirect(URL(r=request, c='static', f='timelapse/'+timelapse.image_folder+ '/latest_image.jpg', vars=dict(t=time.time())))
    elif task.status == "QUEUED":
        #give placeholder image
        redirect(URL(r=request, c='static', f='timelapse/queued.jpg', args=time.time()))
    elif task.status == "COMPLETED":
        #simply redirect to the final image in static
        redirect(URL(r=request, c='static', f='timelapse/'+ timelapse.image_folder+ '/latest_image.jpg'))

def delete():
    timelapse = db.timelapse[request.args[0]]
    user_really_sure = int(request.vars.really_sure or 0)
    print user_really_sure
    if user_really_sure == 1:
        print 'Nuking it!'
        #go ahead and delete the folder and if exists the zip file
        image_directory = os.path.join(request.folder,'static', 'timelapse', timelapse.image_folder)
        if os.path.exists(image_directory):
            shutil.rmtree(image_directory)
        #and zip file too
        zip_filename = timelapse.image_folder+"_timelapse_images.zip"
        zip_file_path = os.path.join(request.folder,'static', 'timelapse', zip_filename)

        #only delete if actually exists
        if os.path.exists(zip_file_path):
            try:
                shutil.rmtree(zip_file_path)
            except:
                print 'WTF we just verified it existed so why is it erroring?'
        
        #and delete from database too
        db(db.scheduler_task.id == timelapse.scheduler_task).delete()
        db(db.timelapse.id == timelapse.id).delete()
        redirect(URL(r=request, f='index'))
    else:
        return dict(timelapse = timelapse)

def schedule_pipic():
    """Schedule a new timelapse that continually adjusts the exposure to try to maintain a target image brightness"""
    form = SQLFORM(db.timelapse, fields = ['name', 'image_folder', 
                    'shot_interval', 'shot_uom', 'duration', 'duration_uom',
                    'width', 'height', 'start_time', 'target_brightness', 'max_brightness_delta',
                    'custom_raspistill_args'])
    if form.process().accepted:
        response.flash = 'form accepted'
        new_id = form.vars.id
        timelapse = db.timelapse[new_id]
        #queue up task for scheduler to run

        #run_timelapse(width = timelapse.width, height = timelapse.height, 
        #         interval = timelapse.shot_interval, interval_uom = timelapse.shot_uom, maxshots = -1,
        #         maxtime = timelapse.duration, maxtime_uom = timelapse.duration_uom, brightness = 128, maxdelta = 128,
        #         base_dir = request.folder, output_directory = timelapse.image_folder, include_exif = False)
        maxtime = interval_to_seconds(timelapse.duration, timelapse.duration_uom)

        task = scheduler.queue_task(
            'run_timelapse',
            pargs=[],
            pvars=dict(width = timelapse.width, height = timelapse.height, 
                 interval = timelapse.shot_interval, interval_uom = timelapse.shot_uom, maxshots = -1,
                 duration = timelapse.duration, duration_uom = timelapse.duration_uom, 
                 brightness = timelapse.target_brightness, maxdelta = timelapse.max_brightness_delta,
                 base_dir = request.folder, output_directory = timelapse.image_folder, include_exif = False,
                 timelapse_id = timelapse.id, custom_raspistill_args = timelapse.custom_raspistill_args),
            start_time=timelapse.start_time, 		#datetime
            stop_time = None,		#datetime
            timeout = maxtime+60,               #seconds
            prevent_drift=False,
            period=60,                  #seconds
            #immediate=True,
            repeats = 1
            )
        
        #save timelapse to database so can monitor/view images later
        timelapse.update_record(scheduler_task = task)
        redirect(URL(r=request, f='view', args=[timelapse.id]))


    elif form.errors:
        response.flash = 'form has errors'
    else:
        response.flash = 'please fill out the form'
    

    return dict(form = form)

def kill_timelapse_scheduler():
    scheduler.kill()
    scheduler.resume()
    redirect(URL(r=request, f='index'))

def preview_images():
    timelapse = db.timelapse[request.args[0]]
    response.view = 'timelapse/preview_images.json'
    if timelapse == None:
        images_to_preview = [dict(filename="../no_images.jpg", stats=None, created_time = None)]
    else:
        try:
            preview_images = int(request.vars.preview_images)
        except:
            preview_images = 10#how many recent images to show in addition to latest
        
        image_directory = os.path.join(request.folder,'static', 'timelapse', timelapse.image_folder)
        if not os.path.exists(image_directory):
            os.makedirs(image_directory)#create the directory now (actual timelapse hasn't started yet)
        image_files = sorted(os.listdir(image_directory))
        images = []
        for image in image_files:
            if image != 'latest_image.jpg':
                stats = os.stat(os.path.join(image_directory, image))
                images.append(dict(
                                filename = image, 
                                size = float(stats.st_size/1024),
                                created_time = datetime.fromtimestamp(stats.st_ctime)
                                )
                              )
        images_to_preview = images[(-1*(int(preview_images))):]
        images_to_preview.reverse()
    return dict(images = images_to_preview, timelapse = timelapse)
