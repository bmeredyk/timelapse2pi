# README #
This project provides a simple front-end for using your Raspberry Pi with camera module to take time lapse images.

### System Requirements ###
* Raspberry Pi board - tested with version 1 B+ and version 2 B
* Rasbian OS installed (probably works with others but untested)
* Camera Module installed and enabled
* A MicroSD card large enough to hold your images and do yourself a favour and make sure it is Class 10. (By default the time lapse images get saved in folders within /home/pi/web2py/timelapse2pi/static/timelapse if you want them saved elsewhere you'll have to edit the source to point to the desired location.)
* *Highly Recommended* a RTC (Real Time Clock) module installed. If you plan on using this to schedule time lapse runs to start at a certain time and your RPI doesn't have internet on location it won't work! By default RPI always gets the current time via NTP over the network. If you want to be able to scheduler a run, shut down your RPI, take it out someplace and then power it back up and expect it to start at the right time then get an RTC. I've had success with this one http://www.amazon.com/SunFounder-DS3231-Precision-Raspberry-Arduino/dp/B00HF4NUSS/ 

### How do I get set up? ###

* Start with a standard install of Raspbian
* Download the latest version of web2py from http://web2py.com/init/default/download. You will want the "Source Code" version. Extract the archive into /home/pi/web2py on your RPI.
* Download or git clone this repo and copy to /home/pi/web2py/applications/timelapse2pi
* From a terminal on your RPI run the following to fire up web2py for the first time:

```
#!bash
python web2py/web2py.py -a SecretPassword --ip=0.0.0.0 &

```
* Now fire up your browser and go to http://your-rpi:8000/timelapse2pi/timelapse/
* To actually get it to take pictures you'll also need to start up the web2py scheduler thusly:

```
#!bash

python web2py/web2py.py -K timelapse2pi &
```


### Setting up As A Service ###
Since you probably won't want to have to manually start both the web2py server and scheduler every time you can set them both up to run as a service. Copy the web2py-scheduler-service.sh and web2py-server-service.sh files from timelapse2pi/private into /etc/init.d


```
#!bash

sudo cp /home/pi/web2py/applications/timelapse2pi/private/web2py-scheduler-service.sh /etc/init.d/web2py-scheduler-service.sh
sudo cp /home/pi/web2py/applications/timelapse2pi/private/web2py-server-service.sh /etc/init.d/web2py-server-service.sh
sudo chmod +x /etc/init.d/web2py-scheduler-service.sh
sudo chmod +x /etc/init.d/web2py-server-service.sh
sudo systemctl daemon-reload
```
Now test out starting your two new services with

```
#!bash

sudo /etc/init.d/web2py-server-service.sh start
sudo /etc/init.d/web2py-scheduler-service.sh start
```
Both should show that they start OK and you should be able to access the app in your web browser.

verify that everything is configured to start on boot by running

```
#!bash

ls -l /etc/rc?.d/*web2py*.sh
```
If everything is good you should see a much of results. If not try running the following.

```
#!bash

sudo update-rc.d web2py-server-service.sh defaults
sudo update-rc.d web2py-scheduler-service.sh defaults
```


Instructions for setting up service based on http://blog.scphillips.com/posts/2013/07/getting-a-python-script-to-run-in-the-background-as-a-service-on-boot/

### Optional ###
By default timelapse2pi just uses the simple python based rocket web server and SQLite for the database. If you want you can install MySQL or PostgreSQL and use those instead - just update models/db.py with the appropriate connection string. You can also use a more powerful web server like Apache or Nginx. Refer to www.web2py.com/book for deployment recipes. However, considering that this is running on a RPI and is really only intended as a quick interface for scheduling time lapse with raspistill and showing you the images so you can point the camera in the right direction doing so is probably overkill.