#!/usr/bin/python

from PIL import Image
import os, sys, argparse
import subprocess
import time
from datetime import datetime
import shutil
import zipfile
from gluon.validators import IS_INT_IN_RANGE
#from gluon.dal.objects import Field


def interval_to_seconds(interval, uom):
    '''converts an interval and a unit of measure to seconds
    valid units of measure are 'second', 'seconds', 'minute',
     'minutes', 'hour', 'hours'
    '''
    uom = uom.lower()#just in case there was capitalization
    #trim any trailing s so can have second or seconds
    if uom[-1] == 's':
        uom = uom[0:-1]

    #convert times to millisecond
    if uom.lower() == 'second':
        interval = interval
    elif uom.lower() == 'minute':
        interval = interval * 60
    elif uom.lower() == 'hour':
        interval = interval * 60 * 60


    return interval

def interval_to_milliseconds(interval, uom):
    '''converts an interval and a unit of measure to seconds
    valid units of measure are 'second', 'seconds', 'minute',
     'minutes', 'hour', 'hours'
    '''
    uom = uom.lower()#just in case there was capitalization
    #trim any trailing s so can have second or seconds
    if uom[-1] == 's':
        uom = uom[0:-1]

    #convert times to seconds
    if uom.lower() == 'second':
        interval = interval
    elif uom.lower() == 'minute':
        interval = interval * 60
    elif uom.lower() == 'hour':
        interval = interval * 60 * 60

    ms = interval * 1000

    return ms

def run_timelapse(width = 1024, height = 768, interval = 10, interval_uom = 'seconds', maxshots = -1,
                 duration = 5, duration_uom = 'minutes', brightness = 128, maxdelta = 128,
                 base_dir = request.folder,
                 output_directory = '', include_exif = False, name=None,
                 timelapse_id = None, custom_raspistill_args=''):

    #directory to output snapshots to
    print base_dir
    root_dir = os.path.abspath(os.path.join(base_dir, 'static', 'timelapse'))
    target_dir = os.path.join(root_dir, output_directory)
    print target_dir
    #see if requested output dir exists and if not create
    if not os.path.exists(target_dir):
        os.makedirs(target_dir)
        image_filename_pattern = '/image_%04d.jpg'
    else:
        #really shouldn't be re-using existing directory. Might accidentally overwrite
        #the images from a previous run. This is known to happen if the rpi gets shutdown
        #accidentally mid-run and the tasks ends up being restarted. (Hey I run it off a battery it happens)

        #do ensure unique, generate image_filename_pattern that includes current datetime
        image_filename_pattern = '/image_'+datetime.now().strftime('%Y%m%d%H%M%S')+'_%04d.jpg'


    #convert times to seconds
    interval = interval_to_seconds(interval, interval_uom)
    maxtime = interval_to_seconds(duration, duration_uom)

    TL = pipic_timelapse(w=width, h=height, interval=interval, maxshots=maxshots,
                maxtime=maxtime, targetBrightness=brightness,
                maxdelta=maxdelta, output_directory = target_dir,
                include_exif=include_exif, custom_raspistill_args=custom_raspistill_args)


    #start taking timelapse images
    TL.timelapser()

    #finish it up by recording that it is done and the number of images captured
    #get contents of directory
    timelapse = db.timelapse[timelapse_id]
    try:
        captured_image_files = len(os.listdir(target_dir)) or 0
    except:
        captured_image_files = 0

    timelapse.update_record(images_captured = captured_image_files,
                            completed_at = datetime.now())
    db.commit()
    return dict(captured_image_files = captured_image_files)

from gluon.scheduler import Scheduler
scheduler = Scheduler(db)

db.define_table('timelapse',
    Field('name', 'string'),
    Field('scheduler_task', 'reference scheduler_task', default=None, requires = IS_NULL_OR(IS_IN_DB(db_scheduler,'scheduler_task.id'))),
    Field('image_folder','string', default = datetime.now().strftime('%Y-%m-%d_%H.%M')),
    Field('shot_interval', 'integer', default = 5, label = T('Interval between images')),
    Field('shot_uom', 'string', default = 'Seconds', requires = IS_IN_SET(('Seconds', 'Minutes', 'Hours')),
          label=T('Interval Unit of Measure')),
    Field('duration', 'integer', default = 5, label = T('Duration')),
    Field('duration_uom', 'string', default = 'Minutes', requires = IS_IN_SET(('Seconds', 'Minutes', 'Hours')),
          label = T('Duration UOM')),
    Field('width', 'integer', default=1920, requires = IS_INT_IN_RANGE(640, 2593), label=T('Image Width')),
    Field('height', 'integer', default=1080, requires = IS_INT_IN_RANGE(480, 1945), label=T('Image Height')),
    Field('start_time', 'datetime', default=datetime.now(), label=T('Start capturing at')),
    Field('created_at', 'datetime', default=datetime.now()),
    Field('completed_at', 'datetime'),
    Field('images_captured', 'integer', default = 0),
    Field('target_brightness', 'integer', default = 90, requires=IS_INT_IN_RANGE(0,256),
          label=T('Target brightness'), comment=T('Try to keep images close to this brightness level. Only relevant for the dynamic exposure mode.')),
    Field('max_brightness_delta', 'integer', default = 128, requires=IS_INT_IN_RANGE(0,256),
          label=T('Max brightness delta'),
          comment=T('Only keep images within this range from the target brightness. Only relevant for the dynamic exposure mode.')),
    Field('custom_raspistill_args', 'string', default = '',
          label=T('Optional Custom Arguments for Raspistill'),
          comment = T('Your own arguments to raspistill if you wish to tweak settings.'))
)

def raspistill_lapse(width=1024, height=768, duration=5, duration_uom='minutes',
                    interval=5, interval_uom='seconds', base_dir = request.folder,
                    output_directory = None,
                    name=None, timelapse_id = None, custom_raspistill_args=''):
    """
    Initialize a raspistill subprocess to take timelapse images at specified interval
    """

    #convert times to seconds
    interval = interval_to_milliseconds(interval, interval_uom)
    maxtime = interval_to_milliseconds(duration, duration_uom)

    #output directory
    print base_dir
    root_dir = os.path.abspath(os.path.join(base_dir, 'static', 'timelapse'))
    target_dir = os.path.join(root_dir, output_directory)
    print target_dir
    #see if requested output dir exists and if not create
    if not os.path.exists(target_dir):
        os.makedirs(target_dir)
        image_filename_pattern = '/image_%04d.jpg'
    else:
        #really shouldn't be re-using existing directory. Might accidentally overwrite
        #the images from a previous run. This is known to happen if the rpi gets shutdown
        #accidentally mid-run and the tasks ends up being restarted. (Hey I run it off a battery it happens)

        #to ensure unique, generate image_filename_pattern that includes current datetime *if*
        #there are already files in the directory (we create the folder itself before starting the
        #actual timelapse so isn't always necessary to add datetime)
        image_files = os.listdir(target_dir)
        if len(image_files) > 0:
            image_filename_pattern = '/image_'+datetime.now().strftime('%Y%m%d%H%M%S')+'_%04d.jpg'
        else:
            image_filename_pattern = '/image_%04d.jpg'

    options='-awb auto --nopreview'
    options+=' --width %s --height %s' % (width, height)
    options+=' --timeout %s' % maxtime
    options+=' --timelapse %s' % interval
    #options+=' -ss '+str(self.currentss)
    #options+=' -ISO '+str(self.currentiso)
    options+=' -o '+target_dir+image_filename_pattern
    latest_image_link = os.path.abspath(os.path.join(target_dir,'latest_image.jpg'))
    options+=' --latest '+latest_image_link
    options+=' '+custom_raspistill_args
    print 'raspistill '+options
    r = subprocess.call('raspistill '+options, shell=True)


    #finish it up by recording that it is done and the number of images captured
    #get contents of directory
    timelapse = db.timelapse[timelapse_id]
    try:
        captured_image_files = len(os.listdir(target_dir)) or 0
    except:
        captured_image_files = 0

    timelapse.update_record(images_captured = captured_image_files,
                            completed_at = datetime.now())
    db.commit()
    return dict(completed = 'yes', r = r, captured_image_files = captured_image_files,
                last_sql = db['_lastsql'])

#borrowed from http://bytes.com/topic/python/answers/851018-how-zip-directory-python-using-zipfile
def recursive_zip(zipf, directory, folder=""):
    for item in os.listdir(directory):
        if os.path.isfile(os.path.join(directory, item)):
            zipf.write(os.path.join(directory, item), folder + os.sep + item)
        elif os.path.isdir(os.path.join(directory, item)):
            recursive_zip(
                zipf, os.path.join(directory, item), folder + os.sep + item)

def list_image_files(timelapse_id):
    """Retrieves all of the image files for the specified timelapse along with their stats
    and returns them as a list of dicts"""
    timelapse = db.timelapse[timelapse_id]
    image_directory = os.path.join(request.folder,'static', 'timelapse', timelapse.image_folder)
    image_files = sorted(os.listdir(image_directory))
    images = []
    for image in image_files:
        stats = os.stat(os.path.join(image_directory, image))
        images.append(dict(
                        filename = image, 
                        stats = stats, 
                        created_time = datetime.fromtimestamp(stats.st_ctime)
                        )
                      )
    return images
